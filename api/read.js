const Mercury = require('@postlight/mercury-parser');
const TurndownService = require('turndown')


module.exports = (req, res) => {
    const {
        query: { url }
    } = req;

    if (typeof(url) !== "string") {
        res.send("Please add a URL. Example: /api/read?url=http://google.com")
        return;
    }

    Mercury.parse(url).then(
        result => {
            const turndownService = new TurndownService();
            const title = result.title;
            const title_marker = "-".repeat(title.length);
            const markdown_content = turndownService.turndown(result.content);
            const full_title = title + "\n" + title_marker + "\n";

            res.send(full_title + "\n" + markdown_content);
        }
    );
}
